# CTF19

Proyecto de Documentación del CTF del Evento de Bitup 2019.

## Index

### Atomics
* [**Hashing Version 1**](challenges/hashing_v1/)
* [**Hashing Version 2**](challenges/hashing_v2/)
* [**Encoding**](challenges/encoding/)
* [**Ascii Mode**](challenges/ascii_mode/)
* [**The Lord of XORings**](challenges/the_lord_of_xorings/)
* [**Magic Words**](challenges/magic_words/)
* [**Grep Harder**](challenges/grep_harder/)

### Reversing
* [**Dont Crack Me**](challenges/dont_patch_me/)
* [**Hatred**](challenges/hatred/)

### Web
* [**Js Defuck**](challenges/js_defuck/)
* [**The Cave**](challenges/the_cave/)
* [**SNASA**](challenges/snasa/)
* [**SNASA2**](challenges/snasa2/)

### Recon and OSINT
* [**The Best Match**](challenges/the_best_match/)

### Forensics
* [**Recover Me**](challenges/recover_me/)
* [**Emotech**](challenges/emotech/)

### Stego
* [**Hide With Stego**](challenges/hide_with_stego/)
* [**Platinum Winter**](challenges/platinum_winter/)

### Misc
* [**Dump And Win**](challenges/dump_and_win_dows/)
* [**Extract And Win**](challenges/extract_and_win_dows/)
* [**Simple Analysis**](challenges/simple_anal/)
* [**The Backdoor**](challenges/the_backdoor/)

## Descripción
Este año se ha planteado el CTF intentando seguir una idea distinta a lo anterior visto: "retos lo más parecidos a la realidad". Y si, puede que alguien encuentre de dudosa realidad algún que otro reto, bueno... no es fácil encajarlo todo bien, y de ahí se aprende para otros años hacerlo mejor.

Bueno, para resumir, cada reto esconde una anécdota, una historia que nos pasó a alguno de los organizadores o que nos contaron. O también, retos que pretendían mostrar un conocimiento concreto que pueda ser útil para todo participante en su dia a dia.

Y para acabar, simplemente decir que espero que todos hayáis disfrutado y os haya despertado cierta curiosidad algun que otro reto.

**Importante:** si resolvisteis un reto y el writeup publicado no de parece s lo qué hicisteis o te parece que tu resolución es diferente, no dudes en contactar con [@Secury](https://t.me/Secury) para añadir tu writeup a reto! Por otro lado, si detectas algun problema o algo que no es correcto, sientete libre de abrir un issue y lo solucionaremos asap!

## Contribuciones

* [The Cave](challenges/the_cave) desarrollado por **Josep Moreno** aka [@JoMoZa](https://t.me/JoMoZa)
* [Platinum Winter](challenges/platinum_winter/) desarrollado por **Carlos** aka [@mrcokito](https://t.me/mrcokito) creador de https://hackinglethani.com
* [Hatred](challenges/hatred/) desarrollado por **Pablo** aka [@Mrp314](https://t.me/Mrp314)
* [Emotech](challenges/emotech/) desarrollado por **Abraham Pasamar** aka [@apasamar](https://t.me/apasamar)
* Retos Restantes desarrollados por **Raul Caro** aka [@Secury](https://t.me/Secury)
* Diseño CTFd desarrollado por **David Verdejo** aka [@Verdej0](https://t.me/Verdej0)