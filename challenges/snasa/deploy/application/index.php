<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>SNASA</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="css/cover.css" />
	</head>
	<body class="text-center">


		<div>
			<a href="https://www.youtube.com/watch?v=NBDA21Vc-5Y" target="_blank">
				<img id="snasa" width="500" height="500" src="images/snasa.png" alt="SNASA">
			</a>
			<h2 class="cover-heading">Hidding your Flirt
			<a style="color:red" href="https://attack.mitre.org/" target="_blank">TTPs</a> 
			from the world!
			</h2>
		</div>
			
		<script src="js/jquery.slim.js"></script>
		<script src="js/boottstrap.js"></script>
		<script src="js/popper.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>
