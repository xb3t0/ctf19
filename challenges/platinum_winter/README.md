# Platinum Winter

## Puntos
`700`

## Pista
"https://www.zdnet.com/article/platinum-apt-hides-backdoor-communication-in-text/" `-350 puntos`

## Flag
`bitup19{_l0sT_}`

## Adjuntos
* [platinum_winter.zip](files/platinum_winter.zip) (MD5: 55c6a9eb7171b48e4f34e5d37d0049e1)

## Deploy
None.

## Descripcion
Kaspersky ha interceptado un documento de una conocida APT, pero solo contiene números que no tienen ningún sentido. ¿O sí?

## Solucion
Al abrir el documento html que contiene el zip, salta una alerta indicando **"the numbers are bad! You gotta get away
from them!"**

![](media/1.png)

Si googleas la frase es una clara referencia a la serie “Lost”, que tiene unos números muy
famosos: 4 8 15 16 23 42.
El fichero contiene números aleatorios, que no tienen ningún significado:

![](media/2.png)

Al mirar el código html, vemos que son todo tablas con identificadores. Hay una tabla que tiene
el identificador **4815162342**:

![](media/3.png)

Cada fila de la tabla tiene 120 columnas:

![](media/4.png)

A simple vista no se puede ver, pero si miramos el html en un editor de textos al final de cada
columna hay un cierto número de espacios:

![](media/5.png)

Esto es el método de esteganografía snow, en el que se ocultan mensajes mediante los espacios.
Si nos fijamos, los tres primeros tienen un espacio, el siguiente 5, el siguiente 1, el siguiente 6,
el siguiente 2, el siguiente 3 y los dos siguientes 1. En total nos sale el número 1115162311.
Si nos vamos a la tabla con id 1115162311, vemos que los atributos de las columnas aparecen
desordenados.

Esto si usamos Firefox solo se va a ver en el código fuente, porque en la vista de
inspeccionar elemento los ordena alfabéticamente automáticamente. Si usamos Chrome si que
se puede ver en inspeccionar elemento.

El caso es que algunos están ordenados alfabéticamente y otros no. Si están ordenados
alfabéticamente es un 1, si no lo están es un 0... Se puede hacer un script sencillito para agilizar:

![](media/6.png)


Al final se obtiene esta cadena:

```
011000100110100101110100011101010111000000110001001110010111101101011111011011000011000001110011010101000101111101111101
```
![](media/7.png)

Y este es el flag, `bitup19{_l0sT_}`

## Notas
Este reto está basado en un hecho real, hace poco el equipo de Karspersky descubrió que estos dos métodos de stego fueron utilizados por Platinum APT para ocultar sus actividades:
https://www.zdnet.com/article/platinum-apt-hides-backdoor-communication-in-text/

De ahí que el nombre del reto se llame platinum Winter: platinum porque lo ha utilizado el grupo
Platinum APT, y y Winter porque uno de los métodos de stego que aparecen en el reto se conoce como snow.

## Referencias
* https://www.zdnet.com/article/platinum-apt-hides-backdoor-communication-in-text/