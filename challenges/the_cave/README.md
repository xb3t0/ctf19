# TheCave

## Puntos
`500`

## Flag
`bitup19{Unexp3ct3d_eval(uat3)}`

## Adjuntos
None

## Deploy
* [thecave_deploy.zip](files/thecave_deploy.zip) (MD5: f7f376d6b1ac5d1c063ec1afe7da51a1)

Recuerda que puedes encontrar todos los archivos del despliegue en [deploy](deploy).

### Requerimientos
Se requiere tener instalados los siguientes programas para poder desplegar el reto:
* **Docker**
* **Docker-Compose**

### Pasos
Para desplegar el reto seguimos los siguientes pasos:

1. Verificamos el hash con el comando `md5sum thecave_deploy.zip` y el hash tiene que coincidir con el especificado en el README del proyecto.
2. Extraemos el contenido del comprimido con `unzip thecave_deploy.zip`.
3. Entramos dentro del directorio y tenemos que ver 3 subdirectorios (application,nginx y php) y un archivo (docker-compose.yml). Entonces simplemente usamos el comando `docker-compose build && docker-compose up` y podrás acceder al reto visitando http://localhost:8083

**Opcional:** puedes cambiar el puerto del reto dentro del archivo de docker-compose, así como demás configuraciones.

## Descripcion
Perro malo!! Este año te quedas en casa!! DONDE VAS?!
El perro de BitUp es muy travieso, se ha llevado la flag de este reto, y se ha ido corriendo al fondo de "la cueva". Le gusta enterrar cosas en "hoyos" donde nadie pueda verlas y luego meterse el dentro. 

Puedes tocarlo, tranqui, no hace nada. https://cave.bitupalicante.com

## Soluciones

### Propuesta
Accedemos a la URL reto, nos encontrado un índice vacio.

![](media/1.png)

Si inspeccionamos el código de la página veremos un `/hole.php` en un comentario.

![](media/2.png)

Si accedemos, vemos una pagina de estética similar, con 4 botones y un título. Podemos leer: `"Welcome to the cave, for this ctf you need"`. Y las cuatro opciones. Cada una de estas llama a un `parámetro GET` con distintos valores que nos arrojan distintos mensajes en la misma página.

Cada uno con una imagen, salvo el tercero. En dicha opción: `"Hack the hacker"`, nos da el siguiente mensaje:

> This page will evaLuate your web hacking skills. Think about what is happening. #bitup19

![](media/3.png)

Esto nos da la primera pista, podríamos sospechar que en algún momento se está ejecutando la función ​`eval()`​ de PHP. Y podríamos llegar a pensar pensar en una inyección de código en el lado del servidor si conseguimos manipular los valores que recibe la función.

Probaremos a introducir varias funciones de php que nos permitan ser ejecutadas. Y nos quedaremos siempre con el mensaje `"Guau! Guau!"`. Únicamente si ponemos como valor `"evaL"` y miramos en los comentarios de la
página veremos una posible pista `"Try harder and fuck the system"`.

Esto nos sugiere que podemos utilizar la función `system()` (o al menos es la idea, puesto que ni siquiera sabemos aún donde puede estar la flag). Pero claro, tenemos algo en el backend que nos esta filtrando las funciones más conocidas para poder listar/leer/escribir ficheros así como todas las que pueden derivar en ejecución de código o similar.

Si buscáramos técnicas de Waf Bypassing con la función system en php (Algo así como `"system function waf bypass"` en google) no tardaremos a llegar al siguiente post:

<a href="https://www.secjuice.com/php-rce-bypass-filters-sanitization-waf/">![](media/4.png)</a>

Donde se nos plantea una situación muy parecida a la que esta sucediendo en este reto. Nos propone varias soluciones entre las que está `la codificación de ciertas funciones a hexadecimal`.

![](media/5.jpeg)


Esto se debe a que PHP admite el concepto de `funciones variables`. Esto significa que si un nombre de variable tiene paréntesis anexos a él, PHP buscará una función con el mismo nombre que lo evaluado por la variable, e intentará ejecutarla.

"system" ​forma parte del conjunto de funciones variables por php.

Este puede ser ejecutado igualmente en un formato como ​ `"hex"("string")` y será ejecutado con normalidad.

system en hex equivale a: `73797374656D`, el string en hexadecimal queda como: `\x73\x79\x73\x74\x65\x6D`

En este caso, vamos a llamar a la función system que reciba un parámetro (el cual también codificamos, no es por nada en especial, pero mola como queda y puede ser interesante de cara a bypassear posibles reglas de archivos y demás).

De hecho también podemos utilizar otras variantes para romper la detección estática y/o literal, simplemente concatenando partes del nombre de la función a la que queremos llamar, por ejemplo: ​`(sy.(st).em)(whoami)`

![](media/6.jpeg)
![](media/7.jpeg)

Dejando atrás la segunda via, vamos a probar nuestro payload codificado con su correspondiente hexadecimal de la función "system" así como el/los parámetro/s pasados, los datos por GET quedarían como: `c="\x73\x79\x73\x74\x65\x6D"($_GET["\x63\x6D\x64"])&cmd=ls`

Esto no termina de funcionar por sí solo! Pues no sabemos cómo está estructurado el código que finalmente ejecuta. Tendremos que poner un nombre de una de los parámetros (también puede ser algo inventado) poner un ​ ";"​ más nuestro "payload". Un ejemplo de inyección de código sería el siguiente:

![](media/8.png)

Existen otro métodos aparte de llamar a la función system, y más formas de llegar a ejecutarla aparte de las mostradas. Podríamos pensar en sacar el valor correspondiente a la funcion system del resultado de llamar a la función `get_defined_functions`, y conociendo la posición del array resultante que refiere a system pero utilizando el su valor asociado a las funciones definidas, aunque son muchas las funciones variables de php que se pueden llamar por este método, seguro que hay más formas de resolver el reto que no tengo en cuenta: `get_defined_functions()[internal][477]($_GET["cmd"])?cmd=ls`

En nuestro caso, por suerte para nosotros, hay que codificar en hex la función de funciones de usuario, resultado un payload tal que: `?c=a; "\x73\x79\x73\x74\x65\x6D"("ls")`

![](media/9.png)

Con esto se consigue listar los archivos del directorio actual, donde, descubrimos un "dog.php" que parece ser interesante. Procedemos a leer su contenido:

![](media/10.png)
![](media/11.png)

Y descubriremos que en un comentario, se haya la flag del reto.

### Alternativa @Devploit
Puedes echarle un vistazo su XOR WAF Bypass en su entrada al blog:
https://devploit.github.io/2019/10/27/bitup19-TheCave.html

### Alternativa @Raxacoricofallapatorius1
Evadiendo colocando la función en otro parametro GET:
`https://cave.bitupalicante.com/hole.php?c=$test%20=$_GET[%22test%22](%27dog.php%27)&test=file_get_contents`

## Notas
Hay mas formas de saltarse el preg_match, concatenacion de strings y codificacion en hexadecimal son algunas de ellas. Ten dichas técnicas en cuenta antes de enfrentarte a un WAF tu sólo.

## Referencias
* https://www.secjuice.com/php-rce-bypass-filters-sanitization-waf/
* https://www.php.net/manual/es/function.eval.php
* https://www.php.net/manual/es/functions.variable-functions.php
* https://securitycongress.euskalhack.org/PDF/tengo_un_perro_que_se_llama_WAF.pdf