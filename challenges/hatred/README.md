# Hatred

## Puntos
`1000`

## Pista
None

## Flag
`bitup19{JaQu3d}`

## Adjuntos
* [Ejecutable](files/hatred.exe) (MD5: 93e148bfcd31ce731674354b28823511)

## Deploy
None

## Descripcion
Consigue la password y utilízala como la flag. Ejemplo: bitup19{password}

## Soluciones

### Propuesta

Existen dos partes del reto.

En la primera parte debemos parchear una función que revisa si el binario no esta siendo debuggeado.
Una vez parcheado esa función ya podremos analizar el flujo del código. La siguiente parte es meter la password y revisar las operaciones que se le hace a cada char.


Primero buscamos el main de la función utilizando strings:



![](challenges/hatred/media/2019-10-25_10h33_47.png)


Una vez detectado el main nos situamos en la función que detecta si no esta siendo debuggeado:


![](challenges/hatred/media/2019-10-25_10h36_05.png)


Es necesario parchear la función para que continue el programa:


![](challenges/hatred/media/2019-10-25_10h36_36.png)

Posteriormente el binario va a funcionar correctamente por lo que añadimos una pass de prueba.
Esa pass de prueba sera pasada a base64 y posteriormente se realizara una serie de operaciones (la función se llama sub_928ec0):


![](challenges/hatred/media/2019-10-25_12h22_37.png)

Lo que se tendra que hacer es conseguir sacar el string que con todas las operaciones devuelva esto: g;jf\"db%

![](challenges/hatred/media/2019-10-25_12h26_29.png)


De esa forma sacaras la pass y llegaras al string good job!!

### Extension por @Krilin4
Puedes seguir la solución a este reto con un gran nivel de detalle en el siguiente post del compañero Krilin4:
https://www.reverseandolo.com/2019/11/reversing-write-up-hatred-bitup19.html